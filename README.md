[![Github](http://img.shields.io/badge/15.0-Gitee-4cb648.svg?style=flat&colorA=8F8F8F)](https://gitee.com/rainbowstudio/odoo_cn/tree/15.0/)
[![Gitee](http://img.shields.io/badge/15.0-Coding-875A7B.svg?style=flat&colorA=8F8F8F)](https://gitee.com/rainbowstudio/wecom)


# 基于Odoo中国本地化模块

> 组件状态说明：
> 
| 状态符号 |  说明  |
| :------: | :----: |
|    ✔     | 已完成 |
|  .....   | 进行中 |
|    ✖     | 未开始 |

## 开源HR和行政管理组件模块介绍及规划

|   #   | 模块名称         | 模块介绍                                                                                                                                                | 状态  |
| :---: | :--------------- | :------------------------------------------------------------------------------------------------------------------------------------------------------ | :---: |
|   1   | hrms_base        | 人力资源管理信息系统基础，对员工档案进行管理（员工资料、员工技能、员工合同、部门、标签），整合和重建odoo Hr的菜单。与企业微信进行了解耦，可以独立安装。 |   ✔   |
|   2   | hrms_recruitment | 人力资源管理信息系统招聘管理，合并'hr_recruitment'菜单。                                                                                                |   ✔   |
|   3   | hrms_holidays    | 人力资源管理信息系统休假管理，合并'hr_holidays'菜单。                                                                                                   |   ✔   |
|   4   | hrms_attendance  | 人力资源管理信息系统出勤管理，合并'hr_attendance'菜单。                                                                                                 |   ✔   |
|   5   | hrms_expense     | 人力资源管理信息系统员工费用管理，合并'hr_expense'菜单。                                                                                                |   ✔   |
|   6   | hrms_empowerment | 人力资源管理信息系统员工员工激励，合并 'gamification' 菜单。                                                                                            |   ✔   |
|   3   | administration   | 行政管理。整合档案借阅、会议室预定、物品领用、物品维修、用车等功能                                                                                      |   ✖   |


